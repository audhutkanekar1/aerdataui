import { Component, OnInit, Input } from '@angular/core';
import { DirectoryDetails } from '../models/DirectoryDetails';

@Component({
  selector: 'app-directorylist',
  templateUrl: './directorylist.component.html',
  styleUrls: ['./directorylist.component.scss']
})
export class DirectoryListComponent implements OnInit {

  @Input() directories: Array<DirectoryDetails>;
  @Input() topCount: number;
  constructor() { }

  ngOnInit() {
  }

}
