export class DirectoryDetails {
    FolderName: string;
    Size: number;
    SizeUnit: string;
}
