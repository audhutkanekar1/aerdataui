import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TopDirectoriesComponent } from './topdirectories.component';

describe('TopDirectoriesComponent', () => {
  let component: TopDirectoriesComponent;
  let fixture: ComponentFixture<TopDirectoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopDirectoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopDirectoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
