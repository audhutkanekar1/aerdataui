import { Component, OnInit } from '@angular/core';
import { TopDirectories } from '../models/request/TopDirectories';
import { DirectoryService } from '../services/directory.service';
import { DirectoryDetails } from '../models/DirectoryDetails';


@Component({
  selector: 'app-topdirectories',
  templateUrl: './topdirectories.component.html',
  styleUrls: ['./topdirectories.component.scss']
})
export class TopDirectoriesComponent implements OnInit {

  isLoading = false;
  errorMsg = '';
  formModel: TopDirectories = new TopDirectories();
  TopCount = 5;
  directoryList: Array<DirectoryDetails>;




  constructor(private directorySvc: DirectoryService) {
    this.formModel.TopCount = 5;
  }

  ngOnInit() {

  }

  public getDirectories(): void {
    this.errorMsg = '';
    if (this.formModel.DirectoryPath !== '' && this.formModel.DirectoryPath !== undefined) {

      this.SetValues();

      this.directorySvc.GetTopDirectories(this.formModel).subscribe(rslt => {
        console.log(rslt);
        if (rslt.length > 0) {
          this.directoryList = rslt.sort(function (obj1, obj2) {
            return obj2.Size - obj1.Size;
          });
        }
        this.isLoading = false;
      },
        error => {
          this.isLoading = false;
          this.handleError(error);
        });

    }
  }

  formatSliderLabel(value: number): string {
    // return value.toString().concat('folders');
    return value.toString();
  }

  private handleError(error: any) {
    if (typeof error.error === 'string') {
      this.errorMsg = error.error;
    } else if (error.error.ModelState !== undefined) {
      error.error.ModelState['directoryRequest.DirectoryPath'].forEach(element => {
        this.errorMsg += element;
      });
    } else {
      this.errorMsg = error.error.Message;
    }
    console.log(error.error.ModelState);
  }

  private SetValues(): void {
    this.isLoading = true;
    this.directoryList = null;
    this.formModel.TopCount = this.TopCount;
    this.errorMsg = '';
  }
}
