import { TestBed } from '@angular/core/testing';

import { DirectoryService } from './directory.service';
import { HttpClient, } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { DirectoryDetails } from '../models/DirectoryDetails';
import { TopDirectories } from '../models/request/TopDirectories';

describe('DirectoryService Test', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let directorySvc: DirectoryService;

  const inputData: TopDirectories = {
    DirectoryPath: 'D:\\Demo',
    TopCount: 2
  };

  let expectedDirectoryData: Array<DirectoryDetails>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DirectoryService]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    directorySvc = TestBed.get(DirectoryService);

  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });


  describe('#GetTopDirectories', () => {

    beforeEach(() => {
      expectedDirectoryData = new Array<DirectoryDetails>();
      const directory1: DirectoryDetails = {
        FolderName: 'TestFolder 1',
        Size: 100,
        SizeUnit: 'MB'
      };

      const directory2: DirectoryDetails = {
        FolderName: 'TestFolder 2',
        Size: 70,
        SizeUnit: 'MB'
      };

      expectedDirectoryData.push(directory1);
      expectedDirectoryData.push(directory2);
    });

    it('should return List one', () => {

      directorySvc.GetTopDirectories(inputData).subscribe(
        DirectoryData => {
          expect(DirectoryData).toEqual(expectedDirectoryData, 'should return 2 directories ');
        }
      );


      const reqs = httpTestingController.match('http://localhost/AerData.API/api/Directory/GetTopDirectories');
      expect(reqs).not.toBeNull();
      expect(reqs.length).toEqual(1);
      const req = reqs[0];
      expect(req.request.method).toEqual('POST');
      // Respond with the mock expectedcountryData
      req.flush(expectedDirectoryData);

    });


  });











});
