import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DirectoryDetails } from '../models/DirectoryDetails';
import { TopDirectories } from '../models/request/TopDirectories';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  resourceURI = 'http://localhost/AerData.API/api/Directory';
  topDirectories: Array<DirectoryDetails>;


  constructor(private http: HttpClient) { }

  public GetTopDirectories(directoyRequest: TopDirectories): Observable<Array<DirectoryDetails>> {
    const endpointUrl = this.resourceURI.concat('/GetTopDirectories');
    const topDirectories$ = this.http.post<Array<DirectoryDetails>>(endpointUrl, directoyRequest, httpOptions);
    // store value to be used by other component
    // topDirectories$.subscribe(rslt => this.topDirectories = rslt);
    return topDirectories$;
  }
}


