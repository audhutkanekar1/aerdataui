import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DirectoryDetails } from '../models/DirectoryDetails';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-directorycompositionchart',
  templateUrl: './directorycompositionchart.component.html',
  styleUrls: ['./directorycompositionchart.component.scss']
})
export class DirectoryCompositionChartComponent implements OnInit, OnChanges {

  @Input() directories: Array<DirectoryDetails>;
  @Input() topCount: number;

  chart = new Chart({
    chart: {
      type: 'pie'
    },
    title: {
      text: ''
    },
    credits: {
      enabled: false
    },
    series: [
    ],
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
  });

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.directories);
    this.loadChart();
  }
  private loadChart(): void {
    if (this.directories !== undefined && this.directories !== null) {
      const series = this.directories.map(folder => ({ name: folder.FolderName, y: folder.Size }));
      this.chart.addSeries({
        name: 'Top ' + this.topCount + ' folders composition',
        data: series
      });

    }
  }
}
