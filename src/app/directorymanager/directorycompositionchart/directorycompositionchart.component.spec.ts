import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectorycompositionchartComponent } from './directorycompositionchart.component';

describe('DirectorycompositionchartComponent', () => {
  let component: DirectorycompositionchartComponent;
  let fixture: ComponentFixture<DirectorycompositionchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectorycompositionchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectorycompositionchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
