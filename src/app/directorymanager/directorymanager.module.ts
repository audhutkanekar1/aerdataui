import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopDirectoriesComponent } from './topdirectories/topdirectories.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule, MatIconModule, MatFormFieldModule,
  MatInputModule, MatListModule, MatProgressSpinnerModule, MatSliderModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DirectoryListComponent } from './directorylist/directorylist.component';
import { ChartModule } from 'angular-highcharts';
import { DirectoryCompositionChartComponent } from './directorycompositionchart/directorycompositionchart.component';


@NgModule({
  declarations: [TopDirectoriesComponent, DirectoryListComponent, DirectoryCompositionChartComponent],
  imports: [
    FlexLayoutModule,
    ChartModule,
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    MatSliderModule,
    FormsModule,
    MatProgressSpinnerModule,
    HttpClientModule,
  ]
})
export class DirectoryManagerModule { }
