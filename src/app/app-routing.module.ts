import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopDirectoriesComponent } from './directorymanager/topdirectories/topdirectories.component';

const routes: Routes = [
  {
    path: 'topdirectories',
    component: TopDirectoriesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
